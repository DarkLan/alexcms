<?php

/**
* 
*/
require_once(ROOT."/models/IndexModel.php"); // підключаємо модель

class IndexController extends IndexModel
{
	private $limited_news = 2;
	private $limited_photos = 2;
	private $limited_products = 2;
	private $limited_users = 2;
	private $limited_search = 2;

	private function SetSettings($name, $value) {
		$this->$name = $value;
	}

	public function actionIndex() {
		$results = $this->getIndex($this->limited_news,$this->limited_photos,$this->limited_products,$this->limited_users);
		
		require_once(ROOT.'/views/index.php');

		return $results;
	}
}
?>