<?php
require_once(ROOT."/models/newsModel.php"); // підключаємо модель

class NewsController extends NewsModel
{
	// Кількість на сторінці (обмеження виборки)
	private $limited = 500;

	public function actionIndex() {
		$results['ITEMS'] = $this->getIndex($this->limited);

		require_once(ROOT.'/views/news.php');
		return TRUE;
	}

	public function actionDetail($id) {
		$results['ITEMS'] = $this->getById($id);

		require_once(ROOT.'/views/news_detail.php');
		
		return $results;

	}
}