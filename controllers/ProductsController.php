<?php
/**
* 
*/

require_once(ROOT."/models/ProductsModel.php"); // підключаємо модель

class ProductsController extends ProductsModel
{
	// Кількість на сторінці (обмеження виборки)
	private $limited = 500;

	public function actionIndex() {
		$results = array();
		$results['ITEMS'] = $this->getIndex($this->limited);
		require_once(ROOT.'/views/products.php');
		return TRUE;
	}
	
	public function actionDetail($id) {
		$results = array();
		$data = ProductsModel::getById($id);
		if ($data === FALSE) {
			require_once(ROOT.'/views/404.php');
			return FALSE;
		} else {
			$results['ITEMS'] = $data;
			require_once(ROOT.'/views/products_detail.php');
			return $results;
		}
	}
}