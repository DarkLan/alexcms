<?php
/**
* 
*/

class Router
{
	private $routes; // тут зберігається масив маршрутів

	// Завантажуємо конструкор, який підключить файл з маршрутами.
	public function __construct() {
		$routPath = ROOT.'/config/route.php';
		$this->routes = require_once($routPath);
	}

	/**
	* Вертає рядок запиту (string)
	*/
	private function get_url() {
		if (!empty($_SERVER["REQUEST_URI"])) {
			return trim($_SERVER["REQUEST_URI"]);
		}
	}

	public function run() {
		$urls = $this->get_url();

		//Знайдемо потрібний запит в масиві маршрутів, визначимо який контроллер потрібно задіята та який метод
		foreach ($this->routes as $url => $path) {
			if (preg_match("~$url~", $urls)) {
				//Сформуєму щось схоже на ЧПУ. Правила його формування знаходиться в route.php
				$temp_controller = preg_replace("~$url~", $path, $urls);
				
				$temp_controller = explode("/", $temp_controller);
				
				array_shift($temp_controller); // видалипе верше пусте значення масиву (тому що перше значення / і після функції explode() отримує 0 значення)
				
				$controllerName = ucfirst(array_shift($temp_controller)).'Controller'; // беремо перший елемент масива та формуємо фактичне ім'я потрібного контроллера

				$actionName = 'action'.ucfirst(array_shift($temp_controller)); // формуємо екшн.
				
				$param = $temp_controller;

				//підключаємо потрібний файл контроллера
				$controllerFile = ROOT.'/controllers/'.$controllerName.'.php';
				
				if (file_exists($controllerFile)) {
					require_once($controllerFile);
				}
				
				// створюємо об'єкт класа
					$controllerObj = new $controllerName;

					//$result = $controllerObj->$actionName($param);
				// параметри відображення
					$result = call_user_func_array(array($controllerObj, $actionName), $param);
				
				// якщо сходження знайдене - перервати цикл
				if ($result != NULL) break;
			}
		}
	}
}