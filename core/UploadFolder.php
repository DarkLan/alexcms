<?php
require_once(ROOT."/config/BD.php"); // підключаємо модель бази
class UploadFolder
{
	public $UPLOAD_PATH_IMG = '/upload/img/'; // шлях, куда завантажувати картинки на сайт а також звітки їх брати модулям. Редагувається в адмінці.

	public function __construct(){
		$BD = BD::getConnection();
		$result = $BD->query('SELECT img_folder as "UPLOAD_PATH_IMG"'
			.' FROM setings '
			.' LIMIT 1');
		$data = $result->fetch(PDO::FETCH_ASSOC);
		if (!empty($data['UPLOAD_PATH_IMG'])) {
			$this->UPLOAD_PATH_IMG = $data['UPLOAD_PATH_IMG'];
		}
	}
}
$obj_img = new UploadFolder;
