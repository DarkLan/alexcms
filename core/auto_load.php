<?php
function auto_load($className) {
	$class = explode('\\', $className);
	//print_r($class);
	echo ROOT.'/'.implode(DIRECTORY_SEPARATOR, $class).'.php'.'<br>';
	switch ($class[0]) {
		case 'core':
			require ROOT.'/'.implode(DIRECTORY_SEPARATOR, $class).'.php';
			break;
		case 'config':
			require ROOT.'/'.implode(DIRECTORY_SEPARATOR, $class).'.php';
			break;
		case 'controllers':
			require ROOT.'/'.implode(DIRECTORY_SEPARATOR, $class).'.php';
			break;
		case 'models':
			require ROOT.'/'.implode(DIRECTORY_SEPARATOR, $class).'.php';
			break;
		default:
			break;
	}
}
?>