<?php
/**
* 
*/
class BD
{
	public static function getConnection() {
		$param_access = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php';
		$params = require($param_access);
		$param_connect = "mysql:host={$params['HOST']};dbname={$params['BD_NAME']}";
		return $db = new PDO(
			$param_connect, 
			$params["USER"], 
			$params["PASSWORD"],
			array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	}
}
