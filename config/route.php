<?php
return array (
	// сформуємо регулярний вираз для формування ЧПУ
	// тут говориться що, в рядку запиту news/ може бути будь яке буква і будь яка їх кількість потім / і будь яка кількість цифр (ідентифікатор)
	"news/([0-9]+)" => 'news/detail/$1', // actionView в NewsController
	"news" => 'news/index', // actionIndex в NewsController

	"products/([0-9]+)" => 'products/detail/$1/', // actionView в ProductsController
	"products" => 'products/index', // actionIndex в ProductsController

	"photos/id=([0-9]+)" => 'photos/detail/$1/', // actionDetail в photosController
	"photos" => 'photos/index', // actionIndex в photosController

	"users/([0-9]+)" => 'users/detail/$1', // actionDetail в UsersController
	"users" => 'users/index', // actionIndex в UsersController
	"auth" => 'users/auth', // actionIndex в UsersController

	"admin/news/id=([0-9]+)" => 'news/detail/$1',
	"admin/news/add" => 'news/add',
	"admin/news" => 'news/index',

	"admin/products/id=([0-9]+)" => 'products/detail/$1',
	"admin/products" => 'products/index',

	"admin/photos/id=([0-9]+)" => 'photos/detail/$1',
	"admin/photos" => 'photos/index',

	"admin/users/id=([0-9]+)" => 'users/detail/$1',
	"admin/users" => 'users/index',

	"admin/" => 'index/index',

	'' => 'index/index'
	);