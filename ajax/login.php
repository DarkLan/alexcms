<?php
sleep(1);
if (isset($_POST) && $_POST["form"] === 'login') {
	require_once($_SERVER['DOCUMENT_ROOT']."/models/authModel.php"); // підключаємо модель авторизації
	$login = trim($_POST["login"]);
	$pass = trim($_POST["pass"]);
	
	if (empty($login)) $errors[] = 'Поле "Логін" обовя\'язково потрібно запомнити!';
	if (empty($pass)) $errors[] = 'Поле "Пароль" обовя\'язково потрібно запомнити!';

	if (isset($errors)) {
		foreach ($errors as $error) {
			echo '<p style="color:red;">'.$error.'</p>';
		}
	} else {
		if (!isset($_COOKIE['id']) && !isset($_COOKIE['login'])) {
			$authModel = new authModel;
			$result = $authModel->getUser($login, $pass);
			
			if ($result != FALSE) {
				session_start();
				$_SESSION['user_id'] = $result['id']; // записуємо сесію
				$_SESSION['login'] = $result['login'];
				if ($result['is_admin'] === '1') {
					$_SESSION['user'] = 'admin';
				} else {
					$_SESSION['user'] = 'user';
					setcookie('user', 'user', time() + 3600, '/'); // а також записуємо 
				}
				
				setcookie('user_id', $result['id'], time() + 3600, '/'); // а також записуємо куки з обмеженням по часу
				setcookie('login', $result['login'], time() + 3600, '/');
				echo '<p style="color:green;">Ви успішно авторзувались. Перейти на головну? <a href="/">так</a></p>';
				if ($result['is_admin'] === '1') {
					echo '<p style="color:green;"><a href="/admin/">Панель адміністратора</a></p>';
				}
			} else {
				echo '<p style="color:red;">Пароль чи логін не вірні.</p>';
			}
		} else {
			echo '<p style="color:green;">Схоже на те, що ви уже авторизовані :-). <a href="/ajax/logout.php">Вийти?</a></p>';
		}
	}
}
