<?php

require_once($_SERVER['DOCUMENT_ROOT']."/config/BD.php"); // підключаємо модель бази

class authModel extends BD
{
	public function getUser($login = FALSE, $pass = FALSE, $id = FALSE) {
		if ($login != FALSE && $pass != FALSE) {
			$results = $this->getConnection()->prepare("SELECT id, login, is_admin FROM users WHERE login = :login AND password = MD5(:pass)"); //підготуємо запит

			$results->execute(array('login' => $login, 'pass'=> $pass)); // виконуємо запит
			
			while ($row = $results->fetch(PDO::FETCH_ASSOC)) { // отримуємо дані
				if ($row != FALSE) return $row;
			}
		}
		return FALSE;
	}

}