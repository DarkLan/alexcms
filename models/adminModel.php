<?php
require_once(ROOT."/config/BD.php"); // підключаємо модель бази

class AdminModel
{
	// повертає массив з елементами
	public static function getIndex($limit) {
		$BD = BD::getConnection();

		$result = $BD->query('SELECT id, title, prev_text, detail_text, author, date, img'
			.' FROM news'
			.' LIMIT '.$limit);

		$i = 0;
		while ($row = $result->fetch()) {
			$data[$i]["ID"] = $row['id'];
			$data[$i]["TITLE"] = $row['title'];
			$data[$i]["DATE"] = $row['date'];
			$data[$i]["IMG"] = $row['img'];
			$data[$i]["PREV_TEXT"] = $row['prev_text'];
			$data[$i]["DETAIL_TEXT"] = $row['detail_text'];
			$data[$i]["AUTHOT"] = $row['author'];
			$i++;
		}
		return $data;
	}

	
	// повертає массив з елементом по ІД
	// Приймає числове значення ІД елемента
	public static function getById($id) {
		$id = intval($id);

		$BD = BD::getConnection();
		
		$result = $BD->query('SELECT id as "ID", title as "TITLE", prev_text as "PREV_TEXT", detail_text as "DETAIL_TEXT", author as "AUTHOR", date, img as "IMG"'
			.' FROM news'
			.' WHERE id='. $id 
			.' LIMIT 1');

			$data = $result->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
}