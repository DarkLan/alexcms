<?php
require_once(ROOT."/config/BD.php"); // підключаємо модель бази

class IndexModel
{
	public function getIndex($limited_news, $limited_photos, $limited_products, $limited_users) {
		$result["NEWS"]["MOD_TITLE"] = 'Новини';
		$result["NEWS"] = self::getNews($limited_news);

		$result["USERS"]["MOD_TITLE"] = 'Користувачі';
		$result["USERS"] = self::getUsers($limited_users);
		
		$result["PRODUCTS"]["MOD_TITLE"] = 'Продукти';
		$result["PRODUCTS"] = self::getProducts($limited_products);

		$result["PHOTOS"]["MOD_TITLE"] = 'Фотографії';
		$result["PHOTOS"] = self::getPhotos($limited_photos);

		return $result;
	}

	private static function getNews($limit) {
		$BD = BD::getConnection();
		require(ROOT.'/models/newsModel.php');
		return NewsModel::getIndex($limit);
	}

	private static function getUsers($limit) {
		$BD = BD::getConnection();
		require(ROOT.'/models/usersModel.php');
		return UserModel::getIndex($limit);
	}

	private static function getProducts($limit) {
		$BD = BD::getConnection();
		require(ROOT.'/models/productsModel.php');
		$ProductsModel = new ProductsModel;
		return $ProductsModel->getIndex($limit);
	}

	private static function getPhotos($limit) {
		$BD = BD::getConnection();
		require(ROOT.'/models/photosModel.php');
		$PhotosModel = new PhotosModel;
		return $PhotosModel->getIndex($limit);
	}
}