<?php

/**
* 
*/

require_once(ROOT."/config/BD.php"); // підключаємо модель бази

class PhotosModel
{
	// повертає массив з елементами
	public function getIndex($limit) {
		$BD = BD::getConnection();

		$result = $BD->query('SELECT users.id, users.name as "user_name", img, date_create'
			.' FROM users RIGHT JOIN photo ON users.id=photo.author'
			.' LIMIT '. $limit);

		$i = 0;
		while ($row = $result->fetch()) {
			$data[$i]["ID"] = $row['id'];
			$data[$i]["DATE_CREATE"] = $row['date_create'];
			$data[$i]["IMG"] = $row['img'];
			$data[$i]["AUTHOR"] = $row['user_name'];
			$i++;
		}

		return $data;
	}

	// повертає массив з елементом по ІД
	// Приймає числове значення ІД елемента
	public static function getById($id) {
		$id = intval($id);

		$BD = BD::getConnection();
		
		$result = $BD->query('SELECT photo.id AS "ID", photo.img AS "IMG", photo.date_create AS "DATE_CREATE", users.id, users.name AS "AUTHOR", photo.author'
			.' FROM photo LEFT JOIN users ON users.id=photo.author'
			.' WHERE photo.id='. $id 
			.' LIMIT 1');

			$data = $result->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
}