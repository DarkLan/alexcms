<?php
require_once(ROOT."/config/BD.php"); // підключаємо модель бази

class UserModel
{
	// повертає массив з елементами
	public static function getIndex($limit) {
		$BD = BD::getConnection();
		$result = $BD->query('SELECT id, name, about_me, reg_date, photo'
			.' FROM users'
			.' ORDER BY RAND()'
			.' LIMIT '.$limit);

		$i = 0;
		while ($row = $result->fetch()) {
			$data[$i]["ID"] = $row['id'];
			$data[$i]["NAME"] = $row['name'];
			$data[$i]["ABOUT"] = $row['about_me'];
			$data[$i]["REG_DATE"] = $row['reg_date'];
			$data[$i]["PHOTO"] = (empty($row['photo']) ? 'no-image.jpg' : $row['photo']);
			$i++;
		}
		return $data;
	}
	
	// повертає массив з елементом по ІД
	// Приймає числове значення ІД елемента
	public static function getById($id) {
		$id = intval($id);

		$BD = BD::getConnection();
		
		$result = $BD->query('SELECT id as "ID", name as "NAME", about_me as "ABOUT", reg_date as "REG_DATE", photo as "PHOTO"'
			.' FROM users'
			.' WHERE id='. $id 
			.' LIMIT 1');

			$data = $result->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
}
?>