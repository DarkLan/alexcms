<section>
	<header class="major">
		<h2>Модуль "Новини"</h2>
	</header>
	<div class="posts">
	<?php foreach ($results['ITEMS'] as $newsList) :?>
		<article>
			<a href="/admin/news/<?=$newsList['ID']?>" class="image"><img src="<?='/upload/img/'.$newsList['IMG']?>" alt="" /></a>
			<h3><?=$newsList['TITLE']?></h3>
			<p><?=$newsList['PREV_TEXT']?></p>
			<ul class="actions">
				<li><a href="/news/<?=$newsList['ID']?>" class="button">Детально...</a></li>
			</ul>
		</article>
	<?php endforeach; ?>
	</div>
</section>