<section>
	<header class="major">
		<h2>Модуль "Товари"</h2>
	</header>
	<div class="posts">
	<?foreach ($result['ITEMS'] as $key => $products) :?>
		<article>
			<a href="/products/<?=$products['ID']?>" class="image"><img src="<?=UPLOAD_PATH_IMG.$products['IMG']?>" alt="" /></a>
			<h3><?=$products['TITLE']?></h3>
			<p><?=$products['PREV_TEXT']?></p>
			<ul class="actions">
				<li><a href="/products/<?=$products['ID']?>" class="button">Детально...</a></li>
			</ul>
		</article>
	<?endforeach?>
	</div>
</section>