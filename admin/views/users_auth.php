<style>
	p {
		margin:0; 
	}
</style>
<section>
	<header class="major">
		<h2>Модуль "Авторизація (AJAX)"</h2>
	</header>
	<div class="features">
		<article>
			<div class="content">
			<div id="result" style="display:none;text-align:center;"></div>
			<form action="" method="POST" name="login_form" id="login">
				<label for="login"></label>
					<input type="text" name="login">
				<label for="pass"></label>
					<input type="password" name="pass">
				<input type="submit" name="login" id="send" value="Авторизуватись">
			</form>
			</div>
		</article>
	</div>
</section>

<script>
$('#login').on('submit',function(event) {
	event.preventDefault();
  	var data = $(this).serialize();
  	var id_form = $(this).get(0).id;
  	var result_div = $('#result');
  	var text_before = 'Відправляю запит...';

  	$.ajax({
        url: '/views/ajax/UsersLogin_ajax.php',
        type: 'POST',
        data: data + '&form='+id_form,
        dataType: 'html',
        beforeSend: function(){
        	result_div.fadeIn(500);
        	result_div.html(text_before);
		},
        success: function (data) {
        	result_div.html(data);
        }
    });
})
</script>