<?php if (count($results['NEWS']) > 0) : ?>
	<section>
		<header class="major">
			<h2>Модуль "Новини"</h2>
		</header>
		<div class="posts">
		<? foreach ($results['NEWS'] as $news) :?>
			<article>
				<a href="/news/<?=$news['ID']?>" class="image"><img src="<?=UPLOAD_PATH_IMG.$news["IMG"]?>" alt="" /></a>
				<h3><?=$news["TITLE"]?></h3>
				<p><?=$news['PREV_TEXT']?></p>
				<ul class="actions">
					<li><a href="/news/<?=$news['ID']?>" class="button">Детально...</a></li>
				</ul>
			</article>
		<?php endforeach; // NEWS foreach end ?>
		</div>
	</section>
<?php endif; ?>

<?php if (count($results['USERS']) > 0) : ?>
	<section>
		<header class="major">
			<h2>Модуль "Користувачі"</h2>
		</header>
		<div class="posts">
		<?php foreach ($results['USERS'] as $user) : ?>
			<article>
				<a href="/users/<?=$user['ID']?>" class="image"><img src="<?=UPLOAD_PATH_IMG.$user["PHOTO"]?>" alt="" /></a>
				<h3><?=$user["NAME"]?></h3>
				<p><?=$user['ABOUT']?></p>
				<ul class="actions">
					<li><a href="/users/<?=$user['ID']?>" class="button">Детально...</a></li>
				</ul>
			</article>
		<?php endforeach; // USERS foreach end ?>
		</div>
	</section>
<?php endif; ?>

<?php if (count($results['PRODUCTS']) > 0) : ?>
	<section>
		<header class="major">
			<h2>Модуль "Товари"</h2>
		</header>
		<div class="posts">
		<? foreach ($results['PRODUCTS'] as $product) : ?>
			<article>
				<a href="/products/<?=$product['ID']?>" class="image"><img src="<?=UPLOAD_PATH_IMG.$product["IMG"]?>" alt="" /></a>
				<h3><?=$product["TITLE"]?></h3>
				<p><?=$product['PREV_TEXT']?></p>
				<p><?=$product['PRICE']?></p>
				<ul class="actions">
					<li><a href="/products/<?=$product['ID']?>" class="button">Детально...</a></li>
				</ul>
			</article>
		<?php endforeach; // PRODUCTS foreach end ?>
		</div>
	</section>
<?php endif; ?>

<?php if (count($results['PHOTOS']) > 0) : ?>
	<section>
		<header class="major">
			<h2>Модуль "Фотографії"</h2>
		</header>
		<div class="posts">
		<?php foreach ($results['PHOTOS'] as $photo) { ?>
			<article>
				<a href="/photos/id=<?=$photo['ID']?>" class="image"><img src="<?=UPLOAD_PATH_IMG.$photo["IMG"]?>" alt="" /></a>
				<h3><?=$photo["AUTHOR"]?></h3>
				<p><?=$photo['DATE_CREATE']?></p>
				<ul class="actions">
					<li><a href="/photos/id=<?=$photo['ID']?>" class="button">Детально...</a></li>
				</ul>
			</article>
		<?php endforeach; // PHOTOS foreach end ?>
		</div>
	</section>
<?php endif; ?>