<section>
	<header class="major">
		<h2>Модуль "Користувачі"</h2>
	</header>
	<div class="posts">
	<? foreach ($result['ITEMS'] as $key => $user) {
		if ($user['PHOTO'] === '') $user['PHOTO'] = 'no-image.jpg';
		?>
		<article>
			<a href="/users/<?=$user['ID']?>" class="image"><img src="<?='/upload/img/'.$user['PHOTO']?>" alt="" /></a>
			<h3><?=$user['NAME']?></h3>
			<p><?=$user['ABOUT']?></p>
			<ul class="actions">
				<li><a href="/users/<?=$user['ID']?>" class="button">Детально...</a></li>
			</ul>
		</article>
	<?}?>
	</div>
</section>