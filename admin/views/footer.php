		</div>
	</div>
	<div id="sidebar">
		<div class="inner">
			<section id="search" class="alt">
				<form method="post" action="#">
					<input type="text" name="query" id="query" placeholder="Search" />
				</form>
			</section>
			<nav id="menu">
				<header class="major">
					<h2>Адмін - меню</h2>
				</header>
				<ul>
					<li><a href="/admin/">Головна</a></li>
					<li>
					<span class="opener">Новини</span>		
						<ul>
							<li><a href="/admin/news/">Всі новини</a></li>
							<li><a href="/admin/news/add">Додати</a></li>
							<li><a href="/admin/news/edit">Редагувати</a></li>
							<li><a href="/admin/news/delete">Видалити</a></li>
						</ul>
					</li>

					<li>
					<span class="opener">Користувачі</span>		
						<ul>
							<li><a href="/admin/users/">Всі користувачі</a></li>
							<li><a href="/admin/users/add">Додати</a></li>
							<li><a href="/admin/users/edit">Редагувати</a></li>
							<li><a href="/admin/users/delete">Видалити</a></li>
						</ul>
					</li>

					<li>
					<span class="opener">Продукти</span>		
						<ul>
							<li><a href="/admin/products/">Всі продукти</a></li>
							<li><a href="/admin/products/add">Додати</a></li>
							<li><a href="/admin/products/edit">Редагувати</a></li>
							<li><a href="/admin/products/delete">Видалити</a></li>
						</ul>
					</li>

					<li>
					<span class="opener">Фотографії</span>		
						<ul>
							<li><a href="/admin/photos/">Всі фотографії</a></li>
							<li><a href="/admin/photos/add">Додати</a></li>
							<li><a href="/admin/photos/edit">Редагувати</a></li>
							<li><a href="/admin/photos/delete">Видалити</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<footer id="footer">
				<p class="copyright">&copy; Всі права належать мені :)</p>
			</footer>
		</div>
	</div>
	</div>
	<script src="/views/js/skel.min.js"></script>
	<script src="/views/js/util.js"></script>
	<script src="/views/js/main.js"></script>
	</body>
</html>