<? 
sleep(2);
if (isset($_POST) && $_POST["form"] === 'login') {
	$login = trim($_POST["login"]);
	$pass = trim($_POST["pass"]);
	if (empty($login)) $errors[] = 'Поле "Логін" обовя\'язково потрібно запомнити!';
	if (empty($pass)) $errors[] = 'Поле "Пароль" обовя\'язково потрібно запомнити!';

	if (isset($errors)) {
		foreach ($errors as $error) {
			echo '<p style="color:red;">'.$error.'</p>';
		}
	} else {
		echo '<p style="color:green;">Все ок!</p>';
	}
}