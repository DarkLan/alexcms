<section>
	<header class="major">
		<h2>Модуль "Новини. Додати новину"</h2>
	</header>
	<div class="posts">
		<article>
		<div id="result" style="display:none;text-align:center;"></div>
			<form name="news_add" id="news_add">
				<label for="title">Назва новини</label>
					<input type="text" name="title">
				<label for="prev_text">Короткий опис новини</label>
					<textarea name="prev_text"></textarea>
				<label for="detail_text">Детальний опис новини</label>
					<textarea name="detail_text"></textarea>
				<label for="author">Автор</label>
					<select name="author">
						<option disabled>Автор новини</option>
						<option value="1" selected>Name1</option>
						<option value="2">Name2</option>
						<option value="3">Name3</option>
						<option value="4">Name4</option>
					</select>
				<input type="submit" name="addnew" value="Додати новину">
			</form>
		</article>
	</div>
</section>

<script>
$(document).on('submit', 'form', function(event) {
	event.preventDefault();
  	var data = $(this).serialize();
  	var id_form = $(this).get(0).id;
  	var result_div = $('#result');
  	var text_before = 'Відправляю запит...';

  	$.ajax({
        url: '/admin/views/ajax/news_ajax.php',
        type: 'POST',
        data: data + '&form='+id_form,
        dataType: 'html',
        beforeSend: function(){
        	result_div.fadeIn(500);
        	result_div.html(text_before);
		},
        success: function (data) {
        	result_div.html(data);
        }
    });
})
</script>