<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/core/session.php');
ini_set('display_errors', 1);
error_reporting(E_ALL);

if (isset($_SESSION['user_id']) && isset($_SESSION['login']) && $_SESSION['user'] == 'admin') {
 		require_once($_SERVER['DOCUMENT_ROOT']."/models/authModel.php"); // підключаємо модель авторизації
			// Підключаємо файли ядра та формуємо деякі константи для зручності
			define('ROOT', $_SERVER['DOCUMENT_ROOT']);
			define('DIR', dirname(__FILE__));
			define('DS', DIRECTORY_SEPARATOR);
			require_once(ROOT."/core/UploadFolder.php");
			define('UPLOAD_PATH_IMG', $obj_img->UPLOAD_PATH_IMG);
			require_once(ROOT.'/admin/core/Router.php');

			require_once(DIR.'/views/header.php');
			// Виклик роутера
			$Router = new Router;
			$Router->run();
			require_once(DIR.'/views/footer.php');
} else {
	$home_url = 'http://' . $_SERVER['HTTP_HOST'] . '/auth/';
  	header('Location: ' . $home_url);
}