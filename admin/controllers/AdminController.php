<?php
require_once(DIR."/models/adminModel.php"); // підключаємо модель

class AdminController extends AdminModel
{
	// Кількість на сторінці (обмеження виборки)
	private $limited = 500;

	public function actionIndex() {
		$results['ITEMS'] = $this->getIndex($this->limited);

		require_once(DIR.DS.'views'.DS.'admin.php');
		return TRUE;
	}

	public function actionNewsList() {
		$results = 1;
		require_once(DIR.DS.'views'.DS.'news.php');
		
		return $results;

	}

	public function actionModuleProductsList() {
		$results = 1;
		require_once(DIR.'/views/products.php');
		
		return $results;

	}

	public function actionModulePhotosList() {
		$results = 1;
		require_once(DIR.'/views/photos.php');
		
		return $results;

	}

	public function actionModuleUsersList() {
		$results = 1;
		require_once(DIR.'/views/users.php');
		
		return $results;

	}

	public function actionNews () {
	}
	public function actionUsers () {
	}
	public function actionProducts () {
	}
	public function actionPhotos () {
	}
}