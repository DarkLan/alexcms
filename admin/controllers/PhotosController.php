<?php
/**
* 
*/
require_once(ROOT."/models/PhotosModel.php"); // підключаємо модель

class PhotosController extends PhotosModel
{
	// Кількість на сторінці (обмеження виборки)
	private $limited = 500;

	public function actionIndex() {
		$results['ITEMS'] = $this->getIndex($this->limited);
		require_once(ROOT.'/views/photos.php');

		return $results;
	}

	public function actionDetail($id) {
		$results['ITEMS'] = $this->getById($id);

		require_once(ROOT.'/views/photos_detail.php');
		
		return $results;
	}
}