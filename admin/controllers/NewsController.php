<?php
require_once(ROOT."/models/newsModel.php"); // підключаємо модель

class NewsController extends NewsModel
{
	// Кількість на сторінці (обмеження виборки)
	private $limited = 500;

	public function actionIndex() {
		$results['ITEMS'] = $this->getIndex($this->limited);

		require_once(ROOT.'/views/news.php');
		return TRUE;
	}

	public function actionDetail($id) {
		$results['ITEMS'] = $this->getById($id);

		require_once(ROOT.'/views/news_detail.php');
		
		return $results;

	}

	public function actionAdd($title=FALSE, $prev_text=FALSE, $detail_text=FALSE, $author=FALSE) {
		$title = 'Новина з адмінки 222';
		$prev_text = 'Опис новини прев.';
		$detail_text = 'Детальний опис новини!';
		$author = '1';
		$img = 'bbc_news_logo.png';
		
		$results = $this->add($title, $img, $prev_text, $detail_text, $author);

		/*if (!empty($title) && !empty($prev_text) && !empty($detail_text) && !empty($author)) {
			$this->add($title, $prev_text, $detail_text, $author);
			$result = 'TRUE';
		} else {
			$result = 'FALSE';
		}*/

		require_once(ROOT.'/views/news_add.php');
		return $results;

	}
}