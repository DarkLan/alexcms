<?php
/**
* 
*/
require_once(ROOT."/models/usersModel.php"); // підключаємо модель

class UsersController extends UserModel
{
	// Кількість на сторінці (обмеження виборки)
	private $limited = 500;

	//виводимо всі елементи з обмешенням виборки, яке встановлене в властивості limited
	public function actionIndex() {
		$results['ITEMS'] = $this->getIndex($this->limited);

		require_once(ROOT.'/views/users.php');
		return TRUE;
	}

	//Виводимо один елемент на сторінку по його ІД в БД
	public function actionDetail($id) {
		$results['ITEMS'] = $this->getById($id);

		require_once(ROOT.'/views/user_detail.php');
		return $results;

	}

	public function actionAuth() {
		$results['ITEMS'] = array();
		require_once(ROOT.'/views/users_auth.php');
		return TRUE;

	}
}