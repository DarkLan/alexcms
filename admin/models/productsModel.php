<?php
require_once(ROOT."/config/BD.php"); // підключаємо модель бази

class ProductsModel
{
	// повертає массив з елементами
	public function getIndex($limit) {
		$BD = BD::getConnection();

		$results = $BD->query('SELECT id, active, title, prev_text, detail_text, price, date, img'
			.' FROM products'
			.' WHERE active=1'
			.' LIMIT '. $limit);

		$i = 0;
		while ($row = $results->fetch()) {
			$data[$i]["ID"] = $row['id'];
			$data[$i]["TITLE"] = $row['title'];
			$data[$i]["PREV_TEXT"] = $row['prev_text'];
			$data[$i]["DETAIL_TEXT"] = $row['detail_text'];
			$data[$i]["PRICE"] = $row['price'];
			$data[$i]["DATE"] = $row['date'];
			$data[$i]["IMG"] = (empty($row['img']) ? 'no-image-product.png' : $row['img']);
			$i++;
		}
		return $data;
	}

	// повертає массив з елементом по ІД
	// Приймає числове значення ІД елемента
	public static function getById($ids) {
		$id = intval($ids);

		$BD = BD::getConnection();
		
		$results = $BD->query('SELECT id AS ID, article AS ARTICLE, active, title AS TITLE, prev_text AS PREV_TEXT, detail_text as DETAIL_TEXT, price AS PRICE, date AS DATE, img AS IMG'
			.' FROM products'
			.' WHERE id='.$id
			.' LIMIT 1');

		$data = $results->fetch(PDO::FETCH_ASSOC);
		if (empty($data)) {
			$data = FALSE;
		} else {
			if (empty($data['IMG'])) $data['IMG'] = 'no-image-product.png';
		}
		return $data;
	}
}