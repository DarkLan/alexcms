<?php
require_once(ROOT."/config/BD.php"); // підключаємо модель бази

class NewsModel
{
	// повертає массив з елементами
	public static function getIndex($limit) {
		$BD = BD::getConnection();

		$results = $BD->query('SELECT id, title, prev_text, detail_text, author, date, img'
			.' FROM news'
			.' ORDER BY date DESC'
			.' LIMIT '.$limit);

		$i = 0;
		while ($row = $results->fetch()) {
			$data[$i]["ID"] = $row['id'];
			$data[$i]["TITLE"] = $row['title'];
			$data[$i]["DATE"] = $row['date'];
			$data[$i]["IMG"] = $row['img'];
			$data[$i]["PREV_TEXT"] = $row['prev_text'];
			$data[$i]["DETAIL_TEXT"] = $row['detail_text'];
			$data[$i]["AUTHOT"] = $row['author'];
			$i++;
		}
		return $data;
	}

	
	// повертає массив з елементом по ІД
	// Приймає числове значення ІД елемента
	public static function getById($id) {
		$id = intval($id);

		$BD = BD::getConnection();
		
		$results = $BD->query('SELECT id as "ID", title as "TITLE", prev_text as "PREV_TEXT", detail_text as "DETAIL_TEXT", author as "AUTHOR", date, img as "IMG"'
			.' FROM news'
			.' WHERE id='. $id 
			.' LIMIT 1');

			$data = $result->fetch(PDO::FETCH_ASSOC);
			return $data;
	}

	public function add($title, $img, $prev_text, $detail_text, $author) {
		$author = intval($author);

		$BD = BD::getConnection();
		
		$results = $BD->query('INSERT INTO news (
			title, img, prev_text, detail_text,author,date)
			VALUES (
				"'.$title.'",
				"'.$img.'",
				"'.$prev_text.'",
				"'.$detail_text.'",
				"'.$author.'",
				NOW()
			)'
			);
		
		return $results;
	}

	public static function setById($id, $title, $prev_text, $detail_text) {
		$id = intval($id);

		$BD = BD::getConnection();
		
		$results = $BD->query('UPDATE news SET title="$title", prev_text="$prev_text", detail_text="$detail_text"'
			. ' WHERE id='. $id
			. ' LIMIT 1');
		
		return $result;
	}

	public static function deleteById($id) {
		$id = intval($id);

		$BD = BD::getConnection();
		
		$results = $BD->query('DELETE FROM news WHERE id="$id" LIMIT 1');
		
		return $results;
	}

}