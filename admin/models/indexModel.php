<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/BD.php"); // підключаємо модель бази

class IndexModel
{
	public function getIndex($limited_news, $limited_photos, $limited_products, $limited_users) {
		$results["NEWS"]["MOD_TITLE"] = 'Новини';
		$results["NEWS"] = self::getNews($limited_news);

		$results["USERS"]["MOD_TITLE"] = 'Користувачі';
		$results["USERS"] = self::getUsers($limited_users);
		
		$results["PRODUCTS"]["MOD_TITLE"] = 'Продукти';
		$results["PRODUCTS"] = self::getProducts($limited_products);

		$results["PHOTOS"]["MOD_TITLE"] = 'Фотографії';
		$results["PHOTOS"] = self::getPhotos($limited_photos);

		return $results;
	}

	private static function getNews($limit) {
		$BD = BD::getConnection();
		require($_SERVER['DOCUMENT_ROOT'].'/models/newsModel.php');
		return NewsModel::getIndex($limit);
	}

	private static function getUsers($limit) {
		$BD = BD::getConnection();
		require($_SERVER['DOCUMENT_ROOT'].'/models/usersModel.php');
		return UserModel::getIndex($limit);
	}

	private static function getProducts($limit) {
		$BD = BD::getConnection();
		require($_SERVER['DOCUMENT_ROOT'].'/models/productsModel.php');
		$ProductsModel = new ProductsModel;
		return $ProductsModel->getIndex($limit);
	}

	private static function getPhotos($limit) {
		$BD = BD::getConnection();
		require($_SERVER['DOCUMENT_ROOT'].'/models/photosModel.php');
		$PhotosModel = new PhotosModel;
		return $PhotosModel->getIndex($limit);
	}
}