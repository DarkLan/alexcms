<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/core/session.php');
ini_set('display_errors', 1);
error_reporting(E_ALL);

// Підключаємо файли ядра
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('DIR', dirname(__FILE__));

require_once(ROOT."/core/UploadFolder.php");
define('UPLOAD_PATH_IMG', $obj_img->UPLOAD_PATH_IMG);
require_once(ROOT.'/core/Router.php');



require_once(DIR.'/views/header.php');
// Виклик роутера
$Router = new Router;
$Router->run();

require_once(DIR.'/views/footer.php');