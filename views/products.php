<section>
	<header class="major">
		<h2>Модуль "Товари"</h2>
	</header>
	<div class="posts">
	<?php foreach ($results['ITEMS'] as $products) :?>
		<article>
			<a href="/products/<?=$products['ID']?>" class="image"><img src="<?php echo UPLOAD_PATH_IMG.$products['IMG']?>" alt="" /></a>
			<h3><?php echo $products['TITLE']?></h3>
			<p><?php echo $products['PREV_TEXT']?></p>
			<ul class="actions">
				<li><a href="/products/<?=$products['ID']?>" class="button">Детально...</a></li>
			</ul>
		</article>
	<?php endforeach; ?>
	</div>
</section>