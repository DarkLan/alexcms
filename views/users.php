<section>
	<header class="major">
		<h2>Модуль "Користувачі"</h2>
	</header>
	<div class="posts">
	<?php foreach ($results['ITEMS'] as $user) :
		if ($user['PHOTO'] === '') $user['PHOTO'] = 'no-image.jpg';
		?>
		<article>
			<a href="/users/<?=$user['ID']?>" class="image"><img src="<?php echo '/upload/img/'.$user['PHOTO']?>" alt="" /></a>
			<h3><?php echo $user['NAME']?></h3>
			<p><?php echo $user['ABOUT']?></p>
			<ul class="actions">
				<li><a href="/users/<?php echo $user['ID']?>" class="button">Детально...</a></li>
			</ul>
		</article>
	<?php endforeach; ?>
	</div>
</section>