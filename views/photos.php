<section>
	<header class="major">
		<h2>Модуль "Фотографії"</h2>
	</header>
	<div class="features">
	<div class="photo-container">
		<?php foreach ($results['ITEMS'] as $photo) : ?>
		  <a href="/photos/id=<?=$photo['ID']?>"><img class="mySlides" src="<?=UPLOAD_PATH_IMG.$photo['IMG']?>" style="width:100%"></a>
		<?php endforeach; ?>
		  <button class="" onclick="plusDivs(-1)">&#10094;</button>
		  <button class="" onclick="plusDivs(1)">&#10095;</button>
	</div>
	<div class="photos">
		<?php foreach ($results['ITEMS'] as $photo) :?>
		  <div class="photo_one">
		  	<a href="/photos/id=<?=$photo['ID']?>"><img src="<?=UPLOAD_PATH_IMG.$photo['IMG']?>" style="width:100%"></a>
		  	<div class="author"><?php echo $photo['AUTHOR']?></div>
		  	<div class="date"><?php echo $photo['DATE_CREATE']?></div>
		  </div>
		<?php endforeach; ?>
	</div>
	</div>
</section>
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>