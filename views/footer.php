		</div>
	</div>
	<div id="sidebar">
		<div class="inner">
			<section id="search" class="alt">
				<form method="post" action="#">
					<input type="text" name="query" id="query" placeholder="Search" />
				</form>
			</section>
			<nav id="menu">
				<header class="major">
					<h2>Меню</h2>
				</header>
				<ul>
					<li><a href="/">Головна</a></li>
					<li><a href="/news/">Всі новини</a></li>
					<li><a href="/users/">Всі користувачі</a></li>
					<li><a href="/products/">Всі продукти</a></li>
					<li><a href="/photos/">Всі фотографії</a></li>
					<li><a href="/auth/">Авторизація</a></li>
				</ul>
			</nav>
			<footer id="footer">
				<p class="copyright">&copy; Всі права належать мені :)</p>
			</footer>
		</div>
	</div>
	</div>
	<script src="/views/js/skel.min.js"></script>
	<script src="/views/js/util.js"></script>
	<script src="/views/js/main.js"></script>
	</body>
</html>