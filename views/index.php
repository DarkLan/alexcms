<?php
/*echo '<pre>';
echo print_r($results['USERS'],2);
echo '</pre>'; */
	if (count($results['NEWS']) > 0) { ?>
		<section>
			<header class="major">
				<h2>Модуль "Новини"</h2>
			</header>
			<div class="posts">
			<?php foreach ($results['NEWS'] as $news) { ?>
				<article>
					<a href="/news/<?php echo $news['ID']?>" class="image"><img src="<?php echo UPLOAD_PATH_IMG.$news["IMG"]?>" alt="" /></a>
					<h3><?php echo $news["TITLE"]?></h3>
					<p><?php echo $news['PREV_TEXT']?></p>
					<ul class="actions">
						<li><a href="/news/<?php echo $news['ID']?>" class="button">Детально...</a></li>
					</ul>
				</article>
			<?php }; // NEWS foreach end ?>
			</div>
		</section>

		<?php } if (count($results['USERS']) > 0) {?>
			<section>
				<header class="major">
					<h2>Модуль "Користувачі"</h2>
				</header>
				<div class="posts">
				<?php foreach ($results['USERS'] as $users) { ?>
					<article>
						<a href="/users/<?php echo $users['ID']?>" class="image"><img src="<?php echo UPLOAD_PATH_IMG.$users["PHOTO"]?>" alt="" /></a>
						<h3><?php echo $users["NAME"]?></h3>
						<p><?php echo $users['ABOUT']?></p>
						<ul class="actions">
							<li><a href="/users/<?php echo $users['ID']?>" class="button">Детально...</a></li>
						</ul>
					</article>
				<?php }; // USERS foreach end ?>
				</div>
			</section>

		<?php } if (count($results['PRODUCTS']) > 0) { ?>
			<section>
				<header class="major">
					<h2>Модуль "Товари"</h2>
				</header>
				<div class="posts">
				<?php foreach ($results['PRODUCTS'] as $products) { ?>
					<article>
						<a href="/products/<?php echo $products['ID']?>" class="image"><img src="<?php echo UPLOAD_PATH_IMG.$products["IMG"]?>" alt="" /></a>
						<h3><?php echo $products["TITLE"]?></h3>
						<p><?php echo $products['PREV_TEXT']?></p>
						<p><?php echo $products['PRICE']?></p>
						<ul class="actions">
							<li><a href="/products/<?php echo $products['ID']?>" class="button">Детально...</a></li>
						</ul>
					</article>
				<?php }; // PRODUCTS foreach end ?>
				</div>
			</section>

		<?php } if (count($results['PHOTOS']) > 0) { ?>
		<section>
			<header class="major">
				<h2>Модуль "Фотографії"</h2>
			</header>
			<div class="posts">
			<?php foreach ($results['PHOTOS'] as $photos) { ?>
				<article>
					<a href="/photos/id=<?php echo $photos['ID']?>" class="image"><img src="<?php echo UPLOAD_PATH_IMG.$photos["IMG"]?>" alt="" /></a>
					<h3><?php echo $photos["AUTHOR"]?></h3>
					<p><?php echo $photos['DATE_CREATE']?></p>
					<ul class="actions">
						<li><a href="/photos/id=<?php echo $photos['ID']?>" class="button">Детально...</a></li>
					</ul>
				</article>
			<?php }; // PHOTOS foreach end ?>
			</div>
		</section>
		<?php }